import Vue from 'vue'
import VueRouter from 'vue-router'

//Adding layouts router.
const BlankLayout = () => import("@/layouts/BlankLayout")
const Layout1 = () => import("@/layouts/backend/Layout-1")

//Adding page content router.
const Dashbord1 = () => import('@/views/backend/Dashboards/Dashbord1')
const Customer = () => import('@/views/backend/Dashboards/Customer') 
const Contract = () => import('@/views/backend/Dashboards/Contract')
const Order = () => import('@/views/backend/Dashboards/Order')
const Calendar = () => import('@/views/backend/Dashboards/Calendar')
const ContractAdd = () => import('@/views/backend/Dashboards/ContractAdd')
const CustomerAdd = () => import('@/views/backend/Dashboards/CustomerAdd')
const CustomerEdit = () => import('@/views/backend/Dashboards/CustomerEdit')
const CustomerView = () => import('@/views/backend/Dashboards/CustomerView')
const OrderDetails = () => import('@/views/backend/Dashboards/OrderDetails')
const OrderNew = () => import('@/views/backend/Dashboards/OrderNew')

//ui elements


//auth elements
const SignIn = () => import('@/views/backend/Auth/SignIn')
const SignUp = () => import('@/views/backend/Auth/SignUp')
const RecoverPassword = () => import('@/views/backend/Auth/RecoverPassword')
const ConfirmMail = () => import('@/views/backend/Auth/ConfirmMail')

//pages elements


// const CommingSoon = () => import('@/views/backend/Pages/CommingSoon')
const Invoice = () => import('@/views/backend/Pages/Invoice')
const AddInvoice = () => import('@/views/backend/Pages/AddInvoice')
const ViewInvoice = () => import('@/views/backend/Pages/ViewInvoice')

//app element
const UserAccountSetting = () =>import('@/views/backend/App/UserManagement/UserAccountSetting')
const UserAdd = () =>import('@/views/backend/App/UserManagement/UserAdd')
const UserList = () =>import('@/views/backend/App/UserManagement/UserList')
const UserProfile = () =>import('@/views/backend/App/UserManagement/UserProfile')
const UserProfileEdit = () =>import('@/views/backend/App/UserManagement/UserProfileEdit')
const Accountsetting =() => import('@/views/backend/Pages/Extrapages/AccountSettings')
const privacypolicy = () => import('@/views/backend/Pages/Extrapages/PrivacyPolicy')
const TermsOfUse = () =>import('@/views/backend/Pages/Extrapages/TermsOfUse')

Vue.use(VueRouter)

const childRoute = () => [
  {
    path: '',
    name: 'layout.dashboard1',
    meta: {  name: 'Dashboard1' },
    component: Dashbord1
  },
  {
    path: 'customer',
    name: 'layout.customer',
    meta: {  name: 'customer' },
    component: Customer
  },
  {
    path: 'contract',
    name: 'layout.contract',
    meta: {  name: 'contract' },
    component: Contract
  },
  {
    path: 'order',
    name: 'layout.order',
    meta: {  name: 'order' },
    component: Order
  },
  {
    path: 'customeradd',
    name: 'customer.customeradd',
    meta: {  name: 'customeradd' },
    component: CustomerAdd
  },
  {
    path: 'customeredit',
    name: 'customer.customeredit',
    meta: {  name: 'customeredit' },
    component: CustomerEdit
  },
  {
    path: 'customerview',
    name: 'customer.customerview',
    meta: {  name: 'customerview' },
    component: CustomerView
  },
  {
    path: 'contractadd',
    name: 'contract.contractadd',
    meta: {  name: 'contractadd' },
    component: ContractAdd
  },
  {
    path: 'ordernew',
    name: 'order.ordernew',
    meta: {  name: 'ordernew' },
    component: OrderNew
  },
  {
    path: 'orderdetails',
    name: 'order.orderdetails',
    meta: {  name: 'orderdetails' },
    component: OrderDetails
  },
  {
    path: 'calendar',
    name: 'layout.calendar',
    meta: {  name: 'calendar' },
    component: Calendar
  },
  {
    path: 'user-add',
    name: 'app.user-add',
    meta: {  name: 'user-add' },
    component:UserAdd
  },
  {
    path: 'user-list',
    name: 'app.user-list',
    meta: {  name: 'User Add' },
    component:UserList
  },
  {
    path: 'user-profile',
    name: 'app.user-profile',
    meta: {  name: 'User Profile' },
    component:UserProfile
  },
  {
    path: 'User-profile-edit',
    name: 'app.user-profile-edit',
    meta: {  name: 'user-profile-edit' },
    component:UserProfileEdit
  },
  {
    path: 'User-account-setting',
    name: 'app.user-Account-setting',
    meta: {  name: 'user account setting' },
    component:UserAccountSetting
  },
  {
    path: 'terms-of-use',
    name: 'app.Termsofuse',
    meta: {  name: 'Termsofuse' },
    component:TermsOfUse
  },
  {
    path: 'privacy-policy',
    name: 'app.privacypolicy',
    meta: {  name: 'privacypolicy' },
    component:privacypolicy
  },
  {
    path: 'account-setting',
    name: 'app.Accountsetting',
    meta: {  name: 'Accountsetting' },
    component:Accountsetting
  },

]
const authchildRoute = () =>[
  {
    path: 'sign-in',
    name: 'auth.login',
    meta: {  name: 'SignIn' },
    component: SignIn
  },
  {
    path: 'sign-up',
    name: 'auth.register',
    meta: {  name: 'SignUp' },
    component: SignUp
  },
  {
    path: 'recover-password',
    name: 'auth.recover-password',
    meta: {  name: 'Recover Password' },
    component: RecoverPassword
  },
  {
    path: 'confirm-mail',
    name: 'auth.confirm-mail',
    meta: {  name: 'Confirm Mail' },
    component: ConfirmMail
  },

]
const pageschildRoute = () =>[
 
]
const extrapageschildRoute = () =>[
  {
    path: 'pages-invoice',
    name: 'pages.invoices',
    meta: {  name: 'Pages Invoice' },
    component: Invoice
  },
  {
    path: 'pages-addinvoice',
    name: 'pages.addinvoices',
    meta: {  name: 'Pages Add Invoice' },
    component: AddInvoice
  },
  {
    path: 'pages-viewinvoice',
    name: 'pages.viewinvoices',
    meta: {  name: 'Pages View Invoice' },
    component: ViewInvoice
  },
]

const routes = [
  {
    path: '/',
    name: '',
    component: Layout1,
    children: childRoute()
  },
  {
    path: '/auth',
    name: 'auth',
    component: BlankLayout,
    children: authchildRoute()
  },
  {
    path: '/pages',
    name: 'pages',
    component: BlankLayout,
    children: pageschildRoute()
  },
  {
    path: '/extra-pages',
    name: 'extra-pages',
    component: Layout1,
    children: extrapageschildRoute()
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.VUE_APP_BASE_URL,
  routes
})

export default router
